<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => [ 'cors', 'json.response', 'device.uuid.check' ] ], function () {

    // Screens Data
    Route::get('/get_splash_data', 'API\ApiController@getSplashData');
    Route::get('/get_dashboard_data', 'API\ApiController@getDashboardData');
    Route::get('/get_milestone_data', 'API\ApiController@getMilestoneData');
    Route::get('/get_flaghistory_data', 'API\ApiController@getFlaghistoryData');
    Route::get('/get_flagrules_data', 'API\ApiController@getFlagrulesData');
    Route::get('/get_flagrules_data/{key}', 'API\ApiController@getFlagrulesDataByKey');
    Route::get('/get_mast_data', 'API\ApiController@getMastData');
    Route::get('/get_gallery_data', 'API\ApiController@getGalleryData');

    Route::post('/post_contact_us', 'API\ApiController@postContactFormData');






    Route::get('/language_files', 'API\LanguageFiles@getLanguageFile');
    Route::get('/milestones', 'API\MilestoneController@getMilestones');
    Route::get('/anthem', 'API\AnthemController@getAnthems');









});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
