<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Anthem;
use Illuminate\Http\Request;

class AnthemController extends Controller
{
    //
    public function getAnthems(Request $request){

        $lang = $request->headers->get('lang');
        $lang = $lang ?? "en";
        $data = Anthem::get(['key', $lang])->first();
        $item = [
            $data["key"] => $data[$lang]
        ];

        return self::success('Anthem', ['lang' => $lang, 'data' => $item]);

    }
}
