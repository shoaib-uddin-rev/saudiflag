<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\ContactUs;
use App\Mail\DemoMail;
use App\Models\FlagRule;
use App\Models\History;
use App\Models\Language;
use App\Models\Mast;
use App\Models\Milestone;
use http\Message\Body;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use function GuzzleHttp\Promise\all;

class ApiController extends Controller
{
    //
    public function getSplashData(Request $request)
    {
        $keys = ['language', 'saudi_flag'];
        $data = Language::whereIn('key', $keys)->get();

        $data = $data->transform(function ($item, $key) {
            return [
                'id' => $item->id,
                'key' => $item->key,
                'en' => $item->en,
                'ar' => $item->ar,
            ];
        });
        return self::success('Splash Data', ['data' => $data]);
    }

    public function getDashboardData(Request $request)
    {
        $lang = $request->headers->get('lang');
        $keys = ['language', 'saudi_flag', 'national_anthem', 'gallery', 'mast', 'flag_rules', 'flag_history', 'milestones'];
        $data = Language::whereIn('key', $keys)->orderBy('sort', 'asc')->get();

        $data = $data->transform(function ($item, $key) use ($lang) {
            return [
                'key' => $item->key,
                'value' => $lang == "en" ? $item->en : $item->ar
            ];
        });

        return self::success('Splash Data', ['data' => $data]);
    }

    public function getFlagrulesData(Request $request){

        $lang = $request->headers->get('lang');
        $keys = ['flag_raising_times', 'royal_flag', 'flag_sizes', 'flag_status', 'position_in_odd', 'position_in_even', 'flag_raising_condition', 'penalties_legislation'];
        $data = Language::whereIn('key', $keys)->orderBy('sort', 'asc')->get();

        $data = $data->transform(function ($item, $key) use ($lang) {
            return [
                'key' => $item->key,
                'value' => $lang == "en" ? $item->en : $item->ar,
                'image' => asset("images/".$item->key.".png")
            ];
        });

        return self::success('Flag Rules Main Screen Data', ['data' => $data]);

    }

    public function getMilestoneData(Request $request)
    {
        $lang = $request->headers->get('lang');
        $data = Milestone::get();

//        $data = $data->transform(function ($item, $key) use ($lang) {
//            return [
//                'id' => $item['id'],
//                'title' => $item['title_'.$lang],
//                'date' => $item['date_'.$lang],
//                'content' => $item['content_'.$lang],
//            ];
//        });

        return self::success('Splash Data', ['data' => $data]);
    }

    public function getFlaghistoryData(Request $request)
    {
        $lang = $request->headers->get('lang');
        $data = History::orderBy('sort', 'asc')->get();

//        $data = $data->transform(function ($item, $key) use ($lang) {
//            return [
//                'id' => $item['id'],
//                'date' => $item['date_'.$lang],
//                'tab' => $item['tabs'],
//                'sort' => $item['sort'],
//                'content' => $item['content_'.$lang],
//            ];
//        });

        return self::success('Splash Data', ['data' => $data]);
    }

    public static function replaceTemplateStrings($data){

        $search  = array(
            '{sun1.gif}',
            '{flagemblem.gif}',
            '{rule1.gif}',
            '{rule2.gif}',
            '{rule3.gif}',
            '{rule4.gif}',
            '{mid-flag.gif}',
            '{building.gif}',
            '{order.gif}',
            '{torn.gif}',
            '{multiple-flags.gif}'

        );

        $replace = array(
            asset('images/sun1.gif'),
            asset('images/flagemblem.gif'),
            asset('images/rule1.gif'),
            asset('images/rule2.gif'),
            asset('images/rule3.gif'),
            asset('images/rule4.gif'),
            asset('images/mid-flag.gif'),
            asset('images/building.gif'),
            asset('images/order.gif'),
            asset('images/torn.gif'),
            asset('images/multiple-flags.gif')

        );

        $ctx = $data;

//        for($i = 0; $i < count($search); $i++){
            $ctx = str_replace($search, $replace, $ctx);
//        }


        return $ctx;

    }

    public function getFlagrulesDataByKey(Request $request, $key){

        $lang = $request->headers->get('lang');
        $data = FlagRule::where('key', $key)->first();

        if(!$data){
            return self::failure('No Data Found');
        }

//        $obj = [
//            'id' => $data->id,
//            'content' => $this->replaceTemplateStrings($data['content_'.$lang])
//        ];

        return self::success('Flag Rules Main Screen Data', ['data' => $data]);

    }

    public function getMastData(Request $request)
    {
//        $lang = $request->headers->get('lang');
//        $keys = ['flag_total_area', 'flag_weight', 'flag_height', 'flag_area'];
//        $languages = Language::whereIn('key', $keys)->orderBy('sort', 'asc')->get();
//
//        $languages = $languages->transform(function ($item, $key) use ($lang) {
//            return [
//                'key' => $item->key,
//                'value'=> $lang == "en" ? $item->en : $item->ar
//                ];
//        });
//
//        $lang = $request->headers->get('lang');
        $data = Mast::orderBy('sort', 'asc')->get();

//        $data = $data->transform(function ($item, $key) use ($lang, $languages) {
//            return [
//                'id' => $item['id'],
//                'title' => $item['title_'.$lang],
//                'flag_height' => $item['flag_height_'.$lang],
//                'flag_area' => $item['flag_area_'.$lang],
//                'flag_weight' => $item['flag_weight_'.$lang],
//                'flag_total_area' => $item['flag_total_area_'.$lang],
//                'location' => $item['location_'.$lang],
//                'latitude' => $item['latitude'],
//                'longitude' => $item['longitude'],
//                'image' => $item['image'],
//                'sort' => $item['sort'],
//                'languages' => $languages
//
//            ];
//        });



        return self::success('Mast Data', ['data' => $data]);
    }

    public function getGalleryData(Request $request){

        $files = \File::files(public_path('images/gallery'));
        $images = [];

        foreach ($files as $file) {
            $images[] = [
                "key" => $file->getRelativePathname(),
                "value" => asset('images/gallery/'. $file->getRelativePathname() )
            ];
        }
        return self::success('Images Data', ['data' => $images]);
    }

    public function postContactFormData(Request $request){
        $data = $request->all();
        Mail::alwaysFrom('sultanatif30@gmail.com');
        Mail::to("sultanatif30@gmail.com")->send(new ContactUs($data));
        return self::success('Contact data', ['data' => $data]);
    }
}
