<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Milestone;
use Illuminate\Http\Request;

class MilestoneController extends Controller
{
    //
    public function getMilestones(Request $request){

        $lang = $request->headers->get('lang');
        $lang = $lang ?? "en";
        $data = Milestone::get(['id', 'title_'.$lang , 'date_'.$lang, 'content_'.$lang]);
        return self::success('Milestones', ['lang' => $lang, 'data' => $data]);

    }
}
