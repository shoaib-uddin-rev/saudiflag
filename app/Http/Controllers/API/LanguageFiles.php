<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguageFiles extends Controller
{
    //

    public function getLanguageFile(Request $request)
    {
        $lang = $request->headers->get('lang');
        $lang = $lang ?? "en";
        $data = Language::get(['id', 'key', $lang]);
        return self::success('Language Variables', ['lang' => $lang, 'data' => $data]);
    }
}
